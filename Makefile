TOMCAT_HOME=/usr/share/tomcat7
LOGGER=org.apache.tools.ant.listener.AnsiColorLogger

all: baklog
	TOMCAT_HOME=$(TOMCAT_HOME) ant -logger $(LOGGER) deploy

war:
	TOMCAT_HOME=$(TOMCAT_HOME) ant -logger $(LOGGER) war

aar:
	TOMCAT_HOME=$(TOMCAT_HOME) ant -logger $(LOGGER) aar

undeploy:
	TOMCAT_HOME=$(TOMCAT_HOME) ant -logger $(LOGGER) undeploy

clean:
	TOMCAT_HOME=$(TOMCAT_HOME) ant -logger $(LOGGER) clean

test:
	perl -w help/services-test.pl

makentest: all test

dbclear:
	@echo "Удаление таблиц"
	cat help/mysql/mysql.sql help/mysql/ls_check-restore.21-11-12.sql | mysql -u score -h 192.168.100.150 -p score

dbinit:
	@echo "Удаление таблиц и заполнение данными ls_check для Мурманска"
	cat help/mysql/mysql.sql help/mysql/ls_check-restore.21-11-12.sql | mysql -u score -h 192.168.100.150 -p score
	@echo "Заполнение данными ls_check для Североморска."
	rm -Rf build/SeverLsCheckDBFImport && mkdir -p build/SeverLsCheckDBFImport
	javac -cp ./WebContent/WEB-INF/lib/xBaseJ.jar -d ./build/SeverLsCheckDBFImport src/org/koi08r/ercscore/tools/SeverLsCheckDBFImport.java
	java  -cp ./WebContent/WEB-INF/lib/xBaseJ.jar:./build/SeverLsCheckDBFImport:./WebContent/WEB-INF/lib/commons-logging-1.1.1.jar:./WebContent/WEB-INF/lib/mysql-connector-java-5.1.21-bin.jar org.koi08r.ercscore.tools.SeverLsCheckDBFImport

dbshow:
	mysql -t --column-names -u score -h 192.168.100.150 -p score < help/mysql/show.sql

log:
	cat logs/ercscore.log

baklog:
	@cp -v logs/ercscore.log logs/ercscore$(shell date +%H%M-%d%m%g).log
