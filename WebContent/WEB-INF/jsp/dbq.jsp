<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Query counters database</title>
<style type="text/css">
		@font-face {
			font-family: UbuntuMono;
			src: url(ttf/ubuntumono.ttf);
		}

		body {
			font-family: UbuntuMono;
		}
		.b_search {
			background: transparent url(img/search-20x20.png) no-repeat center top;
			/* border: none; */
			border-radius:5px;
			border-width:1px;
			border-style:solid;
			border-color:#919191;
			cursor:pointer;
		}
		.b_search:hover {
			background: transparent url(img/_search-20x20.png) no-repeat center top;
		}

		.inp_search {
			border: none;
			padding: 0px;
			background-color: #D1D2CF;
		}

		a:link {
			color: black;
		}
		a:visited {
			color: black;
		}
		a:active {
			color: black;
		}
		a:hover {
			color: black;
		}

        .t_data {
                background-color: #D1D2CF;
        }

		.t_inp {
			border: none;
			padding: 0px;
		}


        .t_header {
        		color: white;
                background-color: #000000;
                -webkit-border-radius: 8px 8px 0 0;
				-moz-border-radius: 8px 8px 0 0;
				border-radius: 8px 8px 0 0;
        }
        .txt_error {
        	color: red;
        }
        .txt_success {
        	color: green;
        }
        td {
        		padding: 5px;
        		vertical-align: top;
        		border-width: 1px;
				border-color: #000000;
                border-style: solid;
        }
        table {
				/* width: 90%; */
        		vertical-align: top;
                border-style: none;
        }
		.raw_content {
			max-height: 300px;
			border: none;
			overflow-y: scroll;
			overflow-x: scroll;
		}
</style>
</head>
<body>
<center>

<c:set var="results_per_page" value="${10}" scope="request" />

<p>
	<a href="/ercscore/dbq">home</a>
	<a href=#bottom>bottom</a>
</p>

<form action="/ercscore/dbq?limit=${results_per_page}" method="get">
<table border="2px">
<tr>
	<td class="t_header">
		Поиск по Лс
	</td>
</tr>
<tr>
	<td class="t_inp">
		<input class="inp_search" type="text" name="ls" value="">
		<input class="b_search" type="submit" value="">
	</td>
</tr>
</table>
</form>

<form action="/ercscore/dbq?limit=${results_per_page}" method="get">
<table border="2px">
<tr>
	<td class="t_header">
		Поиск по источнику
	</td>
</tr>
<tr>
	<td class="t_inp">
		<input class="inp_search" type="text" name="source" value="">
		<input class="b_search" type="submit" value="">
	</td>
</tr>
</table>
</form>

<form action="/ercscore/dbq?limit=${results_per_page}" method="get">
<table border="2px">
<tr>
	<td class="t_header">
		Выбрать всё
	</td>
</tr>
<tr>
	<td class="t_inp">
		<input class="inp_search" type="text" name="all" value="" readonly>
		<input class="b_search" type="submit" value="">
	</td>
</tr>
</table>
</form>

<p>_</p>

<c:if test="${not empty query}">
	Запрос: ${query}
</c:if>
<br>

<c:choose>
	<c:when test="${empty scores}">
		Нет результатов.<br>
	</c:when>
	<c:otherwise>
		<c:set var="itr" value="${0}" scope="request" />
			<c:if test="${rowleft > 0}">
				Результатов в БД (осталось/всего): ${rowleft}/${rownumber}.
				 <a href="/ercscore/dbq?${action}&limit=${results_per_page}&offset=${rownumber-rowleft}">ещё</a><br>
			</c:if>
			Всего результатов на странице: ${fn:length(scores)}<br>
			<p>_</p>
			<table border="2px">
			<c:forEach var="score" items="${scores}">
		  	<tr>
  				<c:set var="itr" value="${itr + 1}" scope="request" />
  				<td class="t_header" colspan="10"> * № ${itr}</td>
  			</tr>
  			<tr>
    			<td class="t_header">Дата обработки</td>
			    <td class="t_header">Источник</td>
			    <td class="t_header">Агент</td>
			    <td class="t_header">Результат</td>
			    <td class="t_header">Лс</td>
			    <td class="t_header">Гв</td>
			    <td class="t_header">Хв</td>
			    <td class="t_header">Гз</td>
			    <td class="t_header">Эл</td>
			    <td class="t_header">Сч</td>
  			</tr>
  			<tr>
			    <td class="t_data">${score.formatDate}</td>
			    <td class="t_data">${score.src}</td>
			    <td class="t_data">${score.agent}</td>
			    <c:choose>
   			    <c:when test="${score.lscheck == 1}">
		    		<td class="t_data txt_success">${score.formatLsCheck}</td>
				</c:when>
				<c:otherwise>
					<td class="t_data txt_error">${score.formatLsCheck}</td>
				</c:otherwise>
				</c:choose>
			    <td class="t_data">${score.ls}</td>
			    <td class="t_data">${score.hw}</td>
			    <td class="t_data">${score.cw}</td>
			    <td class="t_data">${score.gz}</td>
			    <td class="t_data">${score.el}</td>
			    <td class="t_data">${score.sc}</td>
			    <%-- <td class="t_data"><div class="raw_content">${score.formatRaw}</div></td> --%>
  			</tr>
  			<tr>
  				<td class="t_header" colspan="10">Необработанное сообщение</td>
  			</tr>
  			<tr>
  				<td class="t_data" colspan="10"><div class="raw_content">${score.formatRaw}</div></td>
  			</tr>
  			</c:forEach>
  			</table>
	</c:otherwise>
</c:choose>




<%-- score.formatRaw --> escapeXml( java.lang.String) --%>

<p><a name="bottom" href=#>top</a></p>

</center>
</body>
</html>