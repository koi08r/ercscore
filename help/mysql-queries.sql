-- Успешно обработанные SMS.
SELECT COUNT(_sc.id) FROM scores _sc INNER JOIN sources _src ON _sc.src_id = _src.id WHERE (_src.agent = 'S' OR _src.agent = 'Z') AND _sc.ls_check = 1 ORDER BY _sc.ts;

-- Успешно обработанные Email.
SELECT COUNT(_sc.id) FROM scores _sc INNER JOIN sources _src ON _sc.src_id = _src.id WHERE _src.agent = 'M' AND _sc.ls_check = 1 ORDER BY _sc.ts;

-- Принятые Email (без raw).
SELECT _sc.id, _sc.ls_check, _src.src, _src.agent, _sc.ts, _sc.ls, _sc.hw, _sc.cw, _sc.gz, _sc.el, _sc.sc  FROM scores _sc INNER JOIN sources _src ON _sc.src_id = _src.id WHERE _src.agent = 'M' ORDER BY _sc.ts;

-- Повторения
SELECT src_id,ts,ls,hw,cw,gz,el,sc FROM scores WHERE src_id = 961 AND src_id in (SELECT src_id FROM scores GROUP BY src_id HAVING COUNT( src_id ) > 1 ) GROUP BY src_id;

-- Переполнения
SELECT id,ls_check,src_id,ts,ls,hw,cw,gz,el,sc FROM scores WHERE ls = 2147483647 OR ls = 4294967295;

-- RE:
SELECT ls,hw,cw,gz,el,sc FROM scores WHERE ls = 000051018967;


-- Выгрузка для Североморска.
-- last: 4911 | 00000000000000880075 | 2012-11-26 12:08:23
--
-- FIELDS ESCAPED BY '\' OPTIONALLY ENCLOSED BY '"'
SELECT src.agent, src.src, sc.ls, sc.hw, sc.cw, sc.gz, sc.el, sc.sc INTO OUTFILE '/tmp/sever-1202-271112.csv' FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n' FROM scores sc INNER JOIN sources src ON sc.src_id = src.id WHERE sc.ls LIKE '000000000000008_____' ORDER BY sc.ts;