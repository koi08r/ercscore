#!/usr/bin/perl

use MIME::Base64;
use SOAP::Lite +trace => [ transport => \&trace ];
use XML::LibXML;
use Switch;
use utf8;


use constant IMPORT_ERR_LSFOUND => 101;
use constant IMPORT_ERR_HWFOUND => 102;
use constant IMPORT_ERR_CWFOUND => 103;
use constant IMPORT_ERR_GZFOUND => 104;
use constant IMPORT_ERR_ELFOUND => 105;

my $proxy = 'http://192.168.5.190:8080/ercscore/services/ercscore';
my $ns = 'http://ws.ercscore.koi08r.org';

my $raw = "ЛС 000000000124 ГВ31 ХВ12 СЧ11 ЭЛ12";
$raw = "Иди нахрен, сволоч\n";

my $soap = undef;

$soap = SOAP::Lite
        -> default_ns($ns)
        -> proxy($proxy,)
        -> on_fault(sub {
        my ($soap, $res) = @_;
        ref($res) ? print "Error: '",$res->faultstring,"'\n" : print "Error: '", $soap->transport->status, "'\n";
        die "exit";
});


if (!$ARGV[0]||$ARGV[0] eq 'pushScore') {
	print ">>>>>>>>>> Test 'pushScore'\n\n";
	my $soms = $soap->call('pushScore',
        	SOAP::Data->new(name => 'src',value => 'vp.burchenya@erc51.info'),
        	SOAP::Data->new(name => 'agent',value => 'M'),
        	SOAP::Data->new(name => 'ls', value => '888777'),
        	SOAP::Data->new(name => 'hw', value => 31),
        	SOAP::Data->new(name => 'cw', value => 12),
        	SOAP::Data->new(name => 'gz', value => undef),
        	SOAP::Data->new(name => 'el', value => undef),
	        SOAP::Data->new(name => 'sc', value => undef),
        	SOAP::Data->new(name => 'raw', value => $raw, type => 'xsd:string'),
	);
	die $soms->faultcode.": ".$soms->faultstring if ($soms->fault);
	print "Result: '", $soms->result, "'\n\n";
}
elsif (!$ARGV[0]||$ARGV[0] eq 'checkLs') {
	print ">>>>>>>>>> Test 'checkLs'\n\n";
	$soms = $soap->call('checkLs',
        SOAP::Data->new(name => 'ls', value => '000000000124'),
	);
	die $soms->faultcode.": ".$soms->faultstring if ($soms->fault);

	unless ($soms->result) {
        	die "No result or result is empty\n";
	} else {
	        print "Result: ",$soms->result ,"\n";
	        foreach ($soms->paramsall) {
	                print "Res: $_\n";
	        }
	}
}
elsif (!$ARGV[0]||$ARGV[0] eq 'echo') {
	print ">>>>>>>>>> Test 'Echo'\n\n";
	$soms = $soap->call('Echo',
        SOAP::Data->new(name => 'str', value => 'ABC'),
	);
	die $soms->faultcode.": ".$soms->faultstring if ($soms->fault);

	unless ($soms->result) {
        	die "No result or result is empty\n";
	} else {
	        print "Result: ",$soms->result ,"\n";
	        foreach ($soms->paramsall) {
	                print "Res: $_\n";
	        }
	}
}
elsif (!$ARGV[0]||$ARGV[0] eq 'importScore') {
        utf8::encode($raw);
        $raw = encode_base64($raw);
	print ">>>>>>>>>> Test 'importScore'\n\n";
	$soms = $soap->call('importScore',
        	SOAP::Data->new(name => 'src',value => 'vp.burchenya@erc51.info'),
	        SOAP::Data->new(name => 'agent',value => 'M'),
        	# SOAP::Data->new(name => 'raw', value => $raw, type => 'xsd:string'),
        	SOAP::Data->new(name => 'raw', value => $raw ),
	);
	die $soms->faultcode.": ".$soms->faultstring if ($soms->fault);

	binmode STDOUT, ':utf8';
	switch ($soms->result) {
		case IMPORT_ERR_LSFOUND { print "Лс не найден\n" }
		case IMPORT_ERR_HWFOUND { print "Счетчик ГВ не найден\n" }
		case IMPORT_ERR_CWFOUND { print "Счетчик ХВ не найден\n" }
		case IMPORT_ERR_GZFOUND { print "Счетчик ГЗ не найден\n" }
		case IMPORT_ERR_ELFOUND { print "Счетчик ЭЛ не найден\n" }
		else { print "Unknown result\n" }
	}

	print "Result: '", $soms->result, "'\n\n";
} else {
	die "Function not found\n";
}


sub trace {
        my ($xml,$doc);

        print ref($_[0]),"\n";
        print "==============\n";

        foreach ($_[0]->header_field_names) {
                print $_,": ",$_[0]->header($_),"\n";
        }
        print "==============\n";

        $doc = $_[0]->content;
        eval {
                if ($_[0]->header('Content-Type') =~ /text\/xml/i) {
                        my $xdoc = XML::LibXML->load_xml(string => \$doc, keep_blanks => 0);
                        print $xdoc->toString(2);
                } else {
                        print $doc;
                }
        };

        print ($@) if ($@);
        print "\n";
}

1;
