package org.koi08r.ercscore.beans;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;


import org.koi08r.ercscore.parsers.CustomStringEscapeUtils;
import org.koi08r.ercscore.ws.Service;

public class ScoreBean {
	private int num = 0;
	private Timestamp ts = null;
	private String src = null;
	private String agent = null;
	private String ls = null;
	private String hw = null;
	private String cw = null;
	private String gz = null;
	private String el = null;
	private String sc = null;
	private String raw = null;
	private Integer lscheck = null; // TODO: Int ???

	private enum AgentsEnum {
		M,S,Z,A
	}

	public ScoreBean() {}

	public void setNum(int i) {
		num = i;
	}
	public int getNum() {
		return num;
	}

	public void setSrc(String s) {
		src = s;
	}
	public String getSrc() {
		return src;
	}

	public void setAgent(String s) {
		AgentsEnum e = AgentsEnum.valueOf(s);
		switch (e) {
		case M:
			agent = "Email";
			break;
		case S:
			agent = "SMS";
			break;
		case Z:
			agent = "SMS-ZP";
			break;
		case A:
			agent = "Asterisk";
			break;
		default:
			agent = s;
			break;
		}
	}
	public String getAgent() {
		return agent;
	}

	public void setLs(String s) {
		ls = s;
	}
	public String getLs() {
		return ls;
	}

	public void setHw(String s) {
		hw = s;
	}
	public String getHw() {
		return hw;
	}

	public void setCw(String s) {
		cw = s;
	}
	public String getCw() {
		return cw;
	}

	public void setGz(String s) {
		gz = s;
	}
	public String getGz() {
		return gz;
	}

	public void setEl(String s) {
		el = s;
	}
	public String getEl() {
		return el;
	}

	public void setSc(String s) {
		sc = s;
	}
	public String getSc() {
		return sc;
	}

	public void setRaw(String s) {
		raw = s;
	}
	public String getRaw() {
		return raw;
	}

	public void setTs(Timestamp d) {
		ts = d;
	}
	public Timestamp getTs() {
		return ts;
	}

	public void setLscheck(Integer i) {
		lscheck = i;
	}
	public Integer getLscheck() {
		return lscheck;
	}

	// Not beans functionality.
	public String getFormatDate() {
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyy HH:mm:ss");
		return df.format(ts);
	}

	public String getFormatRaw() {
		return CustomStringEscapeUtils.escapeHtml4(raw);
	}

	public String getFormatLsCheck() {
		return Service.StrError(lscheck);
	}

}