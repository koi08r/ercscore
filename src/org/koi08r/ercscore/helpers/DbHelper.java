package org.koi08r.ercscore.helpers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.koi08r.ercscore.beans.ScoreBean;

// http://dev.mysql.com/doc/refman/5.5/en/connector-j-reference-configuration-properties.html

public class DbHelper {

	private static final DbHelper INSTANCE = new DbHelper();
	// Если указано ключевое слово static, то код будет исполнен 1 раз при первом вызове конструктора класса.
	private DataSource ds = null;

	private Logger loger = Logger.getLogger("ercscore");

	public static final DbHelper getInstance() {
		return INSTANCE;
	}

	private DbHelper() {
		try {
			Context initContext = new InitialContext();
			Context envContext  = (Context) initContext.lookup("java:/comp/env");
			ds = (DataSource) envContext.lookup("jdbc/MY");
		} catch (NamingException e) {
			loger.error(e.toString());
			ds = null;
		}
    }

	private Connection getPoolConnection() {
		Connection c = null;
		if (ds != null) {
			try {
				c = ds.getConnection();
			} catch (SQLException e) {
				loger.error(e.toString());
				return null;
			}
		} else {
			loger.error("Data source is null");
		}
	return c;
	}


	public void pushScore(
			int ls_check_result,
			String src,
			String agent,
			String ls,
			String hw,
			String cw,
			String gz,
			String el,
			String sc,
			String raw) throws IllegalStateException {

		PreparedStatement st = null;
		// Statement st = null;
		// Connection c = this.getConnnectionForTest();
		Connection c = this.getPoolConnection();

		if (c == null)
			throw new IllegalStateException("No Db connection found");
		else {
			try {


				loger.info("Execute query: '" + "SELECT SCOREADD (" +
					ls_check_result	+ "," +
					"'" + src + "'," +
					"'" + agent + "'," +
					ls + "," +
					hw + "," +
					cw + "," +
					gz + "," +
					el + "," +
					sc + "," +
					"'" + raw + "')'"
				);
				String q = "SELECT SCOREADD (" +
						"?," +
						"?," +
						"?," +
						ls + "," +
						hw + "," +
						cw + "," +
						gz + "," +
						el + "," +
						sc + "," +
						"?)";
				st = c.prepareStatement(q);
				st.setInt(1, ls_check_result);
				st.setString(2, src);
				st.setString(3, agent);
				st.setString(4, raw);


				/*
				st = c.createStatement();
				String q = "SELECT SCOREADD (" +
						"'" + src + "'," +
						"'" + agent + "'," +
						ls + "," +
						hw + "," +
						cw + "," +
						gz + "," +
						el + "," +
						sc + "," +
						"'" + raw + "')"; 
				loger.info("Execute query: '" + q + "'");
				ResultSet rs = st.executeQuery(q);
				*/
				/*
				st = c.prepareStatement("SELECT SCOREADD (?,?,?,?,?,?,?,?,?)");
				st.setString(1, src);
				st.setString(2, agent);
				st.setString(3, ls);
				st.setString(4, hw);
				st.setString(5, cw);
				st.setString(6, gz);
				st.setString(7, el);
				st.setString(8, sc);
				st.setString(9, raw);
				*/

				ResultSet rs = st.executeQuery();
				if (!rs.next())
					throw new IllegalStateException("Invalid result from sql function");
				rs.getBoolean(1); // Функция всегда возвращает true, т.ч. нам это не надо.
				if (rs != null)
					rs.close();
				st.close();
			} catch (SQLException e) {
				loger.error("Err code: " + e.getErrorCode());
				loger.error(e.toString());
				if (e.getErrorCode() != 1264) { // com.mysql.jdbc.MysqlDataTruncation
					throw new IllegalStateException(e.getMessage());	
				} else {
					throw new IllegalStateException("MysqlDataTruncation");
				}
			} finally {
				try {
					c.close(); c = null; // Return connection to pool.
				} catch (SQLException e) {
					// throw new IllegalStateException("SQL connection close error");
					loger.error(e.toString());
				}
			}
		}

	} // End pushScore method.

	
	public ArrayList <ScoreBean> getScores() throws IllegalStateException {

		ArrayList<ScoreBean> result = null;

		PreparedStatement st = null;
		Connection c = this.getPoolConnection();

		if (c == null)
			throw new IllegalStateException("No Db connection found");

		// LEFT | INNER?: SELECT _sc.id, _sc.ls_check, _src.src, _src.agent, _sc.ts, _sc.ls, _sc.hw, _sc.cw, _sc.gz, _sc.el, _sc.sc, _sc.raw FROM scores _sc LEFT JOIN sources _src ON _sc.src_id = _src.id ORDER BY _sc.ts;
		String q =
				"SELECT _src.src, _src.agent, _sc.ts, _sc.ls, _sc.hw, _sc.cw, _sc.gz, _sc.el, _sc.sc" +
				" FROM scores _sc" +
				" RIGHT JOIN sources _src ON _sc.src_id = _src.id" +
				" ORDER BY _sc.ts";	
		loger.info("Execute query: '" + q + "'");
		try {
			st = c.prepareStatement(q);
			// st.setString(1, "123");
			ResultSet rs = st.executeQuery();

			result = new ArrayList<ScoreBean>();
			while (rs.next()) {
				ScoreBean bean = new ScoreBean();
				bean.setSrc(rs.getString("src"));
				bean.setAgent(rs.getString("agent"));
				bean.setTs(rs.getTimestamp("ts"));
				bean.setLs(rs.getString("ls"));
				bean.setHw(rs.getString("hw"));
				bean.setCw(rs.getString("cw"));
				bean.setGz(rs.getString("gz"));
				bean.setEl(rs.getString("el"));
				bean.setSc(rs.getString("sc"));
				result.add( bean );
			}

			if (rs != null)
				rs.close();
			st.close();
            } catch (SQLException e) {
            		result = null;
            		loger.error(e.toString());
                    throw new IllegalStateException("SQL select error");
            } finally {
            	try {
            		c.close(); c = null; // Return connection to pool.
            	} catch (SQLException e) {
            		//throw new IllegalStateException("SQL connection close error"); // Ошибка если соединение уже закрыто.
            	}
            }

		return result;
	} // End getScores method.


	public int[] checkLs(String _ls) throws IllegalStateException {

		PreparedStatement st = null;
		int result[] = new int[4];
		Integer int_ls;

		if (_ls == null) {
			loger.warn("Ls is null.");
			return null;
		}

		try {
			int_ls = Integer.valueOf(_ls);
		} catch (NumberFormatException e) {
			loger.warn("Can't convert '" + _ls + "' to int.");
			// Return to pool (Без этого заполнение пула?).
			// try { c.close(); c = null; } catch (SQLException e1) {}
			return null;
		}

		Connection c = this.getPoolConnection();

		if (c == null)
			throw new IllegalStateException("No Db connection found");

		String q = "SELECT hw,cw,gz,el FROM ls_check WHERE ls = ?";
		loger.info("Execute query: '" + q + "' == " + _ls);
		try {
			st = c.prepareStatement(q);
			st.setInt(1, int_ls);

			ResultSet rs = st.executeQuery();

			if (!rs.next())
				// throw new IllegalStateException("Invalid result from sql function");
				result = null;
			else {
				result[0] = rs.getInt("hw");
				result[1] = rs.getInt("cw");
				result[2] = rs.getInt("gz");
				result[3] = rs.getInt("el");
			}

			if (rs != null)
				rs.close();
			st.close();
		} catch (SQLException e) {
			loger.error(e.toString());
			throw new IllegalStateException("SQL select error");
		} finally {
			try {
				c.close(); c = null; // Return connection to pool.
			} catch (SQLException e) {
				// throw new IllegalStateException("SQL connection close error"); // Ошибка если соединение уже закрыто.
			}
		}

		return result;
	} // End checkLs method.


	/*
	private Connection getConnnectionForTest() {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://MYSQLHOST:3306/score", "score", "score");
			return conn;
		} catch (Exception e) {
			loger.error(e.toString());
			return null;
		}
	}
	*/

}
