package org.koi08r.ercscore.helpers;

public final class DbLimits {

	/* Source table. */
	public static final int SOURCES_SRC_MAX_LEN = 40;
	public static final int SOURCES_AGENT_MAX_LEN = 1;

	/* Scores table. */
	public static final int SCORES_LS_MAX_LEN = 20;
	public static final int SCORES_HW_MAX_LEN = 10;
	public static final int SCORES_CW_MAX_LEN = 10;
	public static final int SCORES_GZ_MAX_LEN = 10;
	public static final int SCORES_EL_MAX_LEN = 10;
	public static final int SCORES_SC_MAX_LEN = 20;
	public static final int SCORES_RAW_MAX_LEN = 65535;

	public static final boolean checkParams(
		String src,
		String agent,
		String ls,
		String hw,
		String cw,
		String gz,
		String el,
		String sc,
		String raw) {

		if (src != null && src.length() != 0 && src.length() > SOURCES_SRC_MAX_LEN)
			return false;
		if (agent != null && agent.length() != 0 && agent.length() > SOURCES_AGENT_MAX_LEN)
			return false;

		if (ls != null && ls.length() != 0 && ls.length() > SCORES_LS_MAX_LEN)
			return false;
		if (hw != null && hw.length() != 0 && hw.length() > SCORES_HW_MAX_LEN)
			return false;
		if (cw != null && cw.length() != 0 && cw.length() > SCORES_CW_MAX_LEN)
			return false;
		if (gz != null && gz.length() != 0 && gz.length() > SCORES_GZ_MAX_LEN)
			return false;
		if (el != null && el.length() != 0 && el.length() > SCORES_EL_MAX_LEN)
			return false;
		if (sc != null && sc.length() != 0 && sc.length() > SCORES_SC_MAX_LEN)
			return false;
		if (raw != null && raw.length() != 0 && el.length() > SCORES_RAW_MAX_LEN)
			return false;
		return true;
	}
}
