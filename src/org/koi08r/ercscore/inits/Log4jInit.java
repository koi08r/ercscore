package org.koi08r.ercscore.inits;

import java.io.File;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

@WebListener
public class Log4jInit implements ServletContextListener {

    public Log4jInit() {

    }

    public void contextInitialized(ServletContextEvent ev) {
    	String homeDir = ev.getServletContext().getRealPath("/");
    	//File propertiesFile = new File(homeDir,"WEB-INF/log4j.properties");
    	File propertiesFile = new File(
    			homeDir,
    			ev.getServletContext().getInitParameter("log4j-properties-path")
        );
        PropertyConfigurator.configure(propertiesFile.toString());
        Logger loger = Logger.getLogger("ercscore");
        loger.info("contextInitialized");
    }

    public void contextDestroyed(ServletContextEvent arg0) {
    }
	
}
