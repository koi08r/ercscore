package org.koi08r.ercscore.parsers;

import org.apache.commons.lang3.text.translate.AggregateTranslator;
import org.apache.commons.lang3.text.translate.CharSequenceTranslator;
import org.apache.commons.lang3.text.translate.EntityArrays;
import org.apache.commons.lang3.text.translate.LookupTranslator;

public class CustomStringEscapeUtils {

	public static String[][] HTML40_NEWLINE_ESCAPE() { return HTML40_NEWLINE_ESCAPE.clone(); }
	private static final String[][] HTML40_NEWLINE_ESCAPE = {
		{"\n", "<br>"},
		{"\r\n", "<br>"},
	};

	// http://commons.apache.org/lang/api/src-html/org/apache/commons/lang3/StringEscapeUtils.html#line.124
	public static final CharSequenceTranslator ESCAPE_HTML4 =
			new AggregateTranslator(
					new LookupTranslator(EntityArrays.BASIC_ESCAPE()),
					new LookupTranslator(EntityArrays.ISO8859_1_ESCAPE()),
					new LookupTranslator(EntityArrays.HTML40_EXTENDED_ESCAPE()),
					new LookupTranslator(HTML40_NEWLINE_ESCAPE())
					/* Test
					, new LookupTranslator(
							new String[][] {
									{"A", "ZZ"},
									{"B", "YY"},
							}
					)
					*/
			);

	public static final String escapeHtml4(String input) {
		return ESCAPE_HTML4.translate(input);
	}

}
