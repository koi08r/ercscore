package org.koi08r.ercscore.parsers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;


public class Schetchik {

	public static final int LS_PARSE_POS = 0;
	public static final int HW_PARSE_POS = 1;
	public static final int CW_PARSE_POS = 2;
	public static final int GZ_PARSE_POS = 3;
	public static final int EL_PARSE_POS = 4;
	public static final int SC_PARSE_POS = 5;

	private static final Schetchik INSTANCE = new Schetchik();
	private Logger loger = Logger.getLogger("ercscore");
	private static final Pattern p = Pattern.compile("(ЛС|ГВ|ХВ|ГЗ|ЭЛ|СЧ)\\s*(\\d+)");
	private static final Pattern r = Pattern.compile("(?:[^А-Яа-я\\d]+|[\\n\\r\\s]+)");

	private enum ScoresEnum {
		ЛС,ГВ,ХВ,ГЗ,ЭЛ,СЧ
	}

	public static final Schetchik getInstance() {
		return INSTANCE;
	}

	private Schetchik() {
    }

	
	public String[] ParseScore(String txt) {

		String result[] = new String[6];

		// Если спустить вниз и м.б. избавиться от raw, не срабатывает изменение регистра.
		String raw = txt.toUpperCase();
		loger.info("Upper: " + raw);

		// Убираем все лишние символы.
		Matcher replacer = r.matcher(raw);
		raw = replacer.replaceAll(" ");
		loger.info("Replace: " + raw);

		// Ищем показания.
		Matcher m = p.matcher(raw);
		loger.info("Founds: ");
		while (m.find()) {
			loger.info(m.group(1) + ":" + m.group(2));
			ScoresEnum e = ScoresEnum.valueOf(m.group(1));
			// TODO: Score from old.
			switch (e) {
			case ЛС:
				if(result[LS_PARSE_POS]==null) result[LS_PARSE_POS] = m.group(2);
				break;
			case ГВ:
				if(result[HW_PARSE_POS]==null) result[HW_PARSE_POS] = m.group(2);
				break;
			case ХВ:
				if(result[CW_PARSE_POS]==null) result[CW_PARSE_POS] = m.group(2);
				break;			
			case ГЗ:
				if(result[GZ_PARSE_POS]==null) result[GZ_PARSE_POS] = m.group(2);
				break;
			case ЭЛ:
				if(result[EL_PARSE_POS]==null) result[EL_PARSE_POS] = m.group(2);
				break;
			case СЧ:
				if(result[SC_PARSE_POS]==null) result[SC_PARSE_POS] = m.group(2);
				break;
			default:
				break;
			}
		}

		return result;
	}
}
