package org.koi08r.ercscore.srvl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.koi08r.ercscore.beans.ScoreBean;
import org.koi08r.ercscore.parsers.CustomStringEscapeUtils;



// TODO: Connection pool.
/*
try {
	scores = DbHelper.getInstance().getScores();
} catch (IllegalStateException e) {
	scores = null;
	forward_page = "error.jsp";
}
*/

// TODO: Проверка возможности хаков посредством offset.
// TODO: Номера страниц, а не кнопки навигации при пролистовнаии больших наборов результатов.
// TODO: логику навигации при пролистовнаии больших наборов результатов попробовать перенести в jsp.

@WebServlet("/dbq")
public class DBQuery extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DBQuery() {
        super();
    }

    private Logger loger = Logger.getLogger("ercscore");

	private int rowNumber = 0;
	private int rowLeft = 0;
	//private int rowOffset = 0;
	//private int rowLimit = 0;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ArrayList<ScoreBean> scores = null;

		String query = null;
		String action = null;

		// Внутренние переменные класса сохраняются между запросами.
		rowNumber = 0;
		rowLeft = 0;

		String all = request.getParameter("all");
		String ls = request.getParameter("ls");
		String source = request.getParameter("source");
		String off = request.getParameter("offset");
		Integer offset = null;
		String lmt = request.getParameter("limit");
		Integer limit = null;

		try {
			if (off == null) offset = 0;
			else offset = Integer.valueOf(off);
		} catch (NumberFormatException e) {
			offset = 0;
		}

		try {
			if (lmt == null) limit = 10;
			else limit = Integer.valueOf(lmt);
		} catch (NumberFormatException e) {
			limit = 10;
		}

		if (source != null && source.length() > 0) {
			query = "(Поиск по источнику) " + source;
			action = "source=" + source;
			try {
				scores = search("SRC",source,offset,limit);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
				scores = null;
			}
		} else if (ls != null && ls.length() > 0) {
			query = "(Поиск по Лс) " + ls;
			action = "ls=" + ls;
			try {
				scores = search("LS",ls,offset,limit);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
				scores = null;
			}
		} else if (all != null) {
			query = "(Выбрать всё) " + all;
			action = "all=" + all;
			try {
				scores = search("ALL",null,offset,limit);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
				scores = null;
			}
		}

		if (query != null) {
			request.setAttribute("query", CustomStringEscapeUtils.escapeHtml4(query));
			request.setAttribute("action", action);
		}
		if (scores != null) request.setAttribute("scores", scores);

		request.setAttribute("rownumber", rowNumber);
		//request.setAttribute("rowlimit", rowLimit);
		request.setAttribute("rowleft", rowLeft);
		//request.setAttribute("rowoffset", rowOffset);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/dbq.jsp");
        dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private ArrayList <ScoreBean> search(String method, String s, int offset, int limit) throws ClassNotFoundException, SQLException {
		boolean all = false;
		String q = null;
		ArrayList<ScoreBean> result = new ArrayList<ScoreBean>();
		Connection c = null;
		PreparedStatement st = null;
		Statement stc = null;
		ResultSet rs = null;

		loger.info("Execute search() with parameters set:" + "\nMethod: " + method + "\nOffset: " + offset + "\nLimit: " + limit);

		if (method.equalsIgnoreCase("SRC"))
			q = "SELECT SQL_CALC_FOUND_ROWS sc.ts,src.src,src.agent,sc.ls_check,sc.ls,sc.hw,sc.cw,sc.gz,sc.el,sc.sc,sc.raw FROM scores sc INNER JOIN sources src ON sc.src_id = src.id WHERE src.src = ? ORDER BY sc.ts LIMIT ?,?";
		else if (method.equalsIgnoreCase("LS"))
			q = "SELECT SQL_CALC_FOUND_ROWS sc.ts,src.src,src.agent,sc.ls_check,sc.ls,sc.hw,sc.cw,sc.gz,sc.el,sc.sc,sc.raw FROM scores sc INNER JOIN sources src ON sc.src_id = src.id WHERE sc.ls = ? ORDER BY sc.ts LIMIT ?,?";
		else if (method.equalsIgnoreCase("ALL")) {
			all = true;
			q = "SELECT SQL_CALC_FOUND_ROWS sc.ts,src.src,src.agent,sc.ls_check,sc.ls,sc.hw,sc.cw,sc.gz,sc.el,sc.sc,sc.raw FROM scores sc INNER JOIN sources src ON sc.src_id = src.id ORDER BY sc.ts LIMIT ?,?";
		} else
			throw new IllegalArgumentException();

		Class.forName("com.mysql.jdbc.Driver");
		c = DriverManager.getConnection("jdbc:mysql://192.168.100.150:3306/score?user=score&password=score&useUnicode=true&characterEncoding=UTF-8&jdbcCompliantTruncation=false");
		st = c.prepareStatement(q);
		// PreparedStatement stat = c.prepareStatement(q, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
		// st.setFetchSize(100);

		if (all) {
			st.setInt(1, offset);
			st.setInt(2, limit);
		} else {
			st.setString(1, s);
			st.setInt(2, offset);
			st.setInt(3, limit);
		}
		rs = st.executeQuery();



		int i = 0;
		while (rs.next()) {
			ScoreBean bean = new ScoreBean();
			bean.setNum(i++); // Номер бина
			bean.setTs(rs.getTimestamp("ts"));
			bean.setSrc(rs.getString("src"));
			bean.setAgent(rs.getString("agent"));
			bean.setLscheck(rs.getInt("ls_check"));
			bean.setLs(rs.getString("ls"));
			bean.setHw(rs.getString("hw"));
			bean.setCw(rs.getString("cw"));
			bean.setGz(rs.getString("gz"));
			bean.setEl(rs.getString("el"));
			bean.setSc(rs.getString("sc"));
			bean.setRaw(rs.getString("raw"));
			result.add( bean );
		}


		if (rs != null) rs.close();
		st.close();

		/* Сколько всего записей. */
		stc = c.createStatement();
		rs = stc.executeQuery("SELECT FOUND_ROWS() AS rows");
		if (rs.next()) rowNumber = rs.getInt("rows");
		if (rs != null) rs.close();
		stc.close();

		// rowLimit = limit;
		// rowOffset = offset;

		/* Высчитываем сколько записей еще не получено. */
		if (result.size() == 0) // Если offset overflow (не доверяем клиенту) или нет результатов.
			rowLeft = 0;
		else
			rowLeft = rowNumber - offset - result.size();


		c.close();
		return result;
	}

}
