package org.koi08r.ercscore.tests;

import org.koi08r.ercscore.parsers.CustomStringEscapeUtils;

public class CommonsLangEscapes {

	public static void main(String[] args) {
		String s = "<html>\n<body>\n<b>H</b>ello,\r\n<i>koi8-r</i>\n<body>\n</html>";
		System.out.println(CustomStringEscapeUtils.escapeHtml4(s));
	}

}
