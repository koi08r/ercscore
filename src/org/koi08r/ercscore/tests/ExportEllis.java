package org.koi08r.ercscore.tests;

import java.io.FileOutputStream;
import java.io.IOException;

import com.linuxense.javadbf.DBFField;
import com.linuxense.javadbf.DBFWriter;

public class ExportEllis {


	/*
		LS@N(12)
		DP@D(8)
		SP@N(15,2)
		P1@N(10)
		P2@N(10)
		P3@N(10)
		P4@N(10)
		POST_TEXT@C(20)
		DT_SMS@C(25)
	
	 */
	public static void main(String[] args) throws IOException {

		System.out.println("Start");

		DBFField fields[] = new DBFField[2];

		fields[0] = new DBFField();
	    fields[0].setName("name");
	    fields[0].setDataType(DBFField.FIELD_TYPE_C);
	    fields[0].setFieldLength(10);

	    fields[1] = new DBFField();
	    fields[1].setName("value");
	    fields[1].setDataType(DBFField.FIELD_TYPE_N);
	    fields[1].setFieldLength(12);
	    fields[1].setDecimalCount(0);

	    DBFWriter writer = new DBFWriter();
	    writer.setCharactersetName("cp1251"); 
	    writer.setFields(fields);


	    Object rowData[] = new Object[2];
	    rowData[0] = "first";
	    rowData[1] = new Double(1);
	    writer.addRecord(rowData);

	    rowData = new Object[2];
	    rowData[0] = "second";
	    rowData[1] = new Double(2);
	    writer.addRecord(rowData);

	    rowData = new Object[2];
	    rowData[0] = "ТРЕТИЙ";
	    rowData[1] = new Double(3);
	    writer.addRecord(rowData);

	    FileOutputStream fos = new FileOutputStream("/tmp/scores2ellis.dbf");
	    writer.write(fos);
	    fos.close();

	    System.out.println("End");

	}

}
