package org.koi08r.ercscore.tests;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* http://docs.oracle.com/javase/6/docs/api/java/util/regex/Pattern.html */

public class RegExp {

	private static String str = "ЛС                874511              hhhhh ,.\\ \n\r    Гв1 ГЗ              11";

	public static void main(String[] args) {
		// Pattern p = Pattern.compile("(ЛС|ГВ|ХВ|ГЗ)\\s*(\\d+)", Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
		Pattern p = Pattern.compile("(ЛС|ГВ|ХВ|ГЗ)\\s*(\\d+)");
		Pattern r = Pattern.compile("(?:[^А-Яа-я\\d]+|[\\n\\r\\s]+)");
		// Pattern r = Pattern.compile("(?:[^\\p{L}\\d]+|[\\n\\r\\s]+)");

		Matcher replacer = r.matcher(str);
		str = str.toUpperCase();
		//System.out.println(str.toUpperCase());
		System.out.println("Replace: " + replacer.replaceAll(" "));
		
		Matcher m = p.matcher(str);

		System.out.println("Founds: ");
		while (m.find()) {
			System.out.println(m.group(1) + ":" + m.group(2));
		}

	}

}
