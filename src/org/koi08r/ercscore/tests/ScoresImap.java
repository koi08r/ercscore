package org.koi08r.ercscore.tests;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeUtility;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.ReceivedDateTerm;
import javax.mail.search.SearchTerm;

import com.sun.mail.imap.IMAPFolder;


public class ScoresImap {

	private static Store store = null;

	public static void main(String[] args) throws MessagingException, UnsupportedEncodingException {

		byte p[] = { 0x5a, 0x6c, 0x6f, 0x62, 0x61, 0x52, 0x75, 0x73, 0x38, 0x33, 0x5f };

		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");
		Session session = Session.getDefaultInstance(props, null);
		store = session.getStore("imaps");

		store.connect("imap.gmail.com", "koi08r@gmail.com", new String(p, "ISO-8859-1"));

		IMAPFolder inbox = (IMAPFolder) store.getFolder("Inbox");
		inbox.open(Folder.READ_ONLY);

		Calendar cal = Calendar.getInstance();
		cal.set(2012, Calendar.NOVEMBER, 26);
		SearchTerm newerThan = new ReceivedDateTerm(ComparisonTerm.GT, cal.getTime());

		System.out.println(cal.getTime().toString());
		Message messages[] = inbox.search(newerThan);

		for (int i=0; i<messages.length; i++) {
			System.out.println("From: " + MimeUtility.decodeText( messages[i].getFrom()[0].toString()));
			System.out.println("To: " + MimeUtility.decodeText(messages[i].getAllRecipients()[0].toString()));
			System.out.println("Date: " + messages[i].getReceivedDate());
			System.out.println("Flags: ");
			// messages[i].setFlag(Flags.Flag.SEEN, true);
			// System.out.println("Body: \n"+ message.getContent());
			System.out.println("Content type: " + messages[0].getContentType());

			System.out.println("==============================");
		}
		inbox.close(true);
		store.close();

	}

}
