package org.koi08r.ercscore.tests;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ScoresLastTest {

	public static final int LS_PARSE_POS = 0;
	public static final int HW_PARSE_POS = 1;
	public static final int CW_PARSE_POS = 2;
	public static final int GZ_PARSE_POS = 3;
	public static final int EL_PARSE_POS = 4;
	public static final int SC_PARSE_POS = 5;

	private static String mail =
			"ЛС000031002320 ХВ39 ГВ44 ГЗ16\n" +
			"ЛС000031002320 ХВ29 ГВ34 ГЗ15\n" +
			"ЛС000031002320 ХВ27 ГВ34 ГЗ14\n";

	private static final Pattern p = Pattern.compile("(ЛС|ГВ|ХВ|ГЗ|ЭЛ|СЧ)\\s*(\\d+)");
	private static final Pattern r = Pattern.compile("(?:[^А-Яа-я\\d]+|[\\n\\r\\s]+)");

	private enum ScoresEnum {
		ЛС,ГВ,ХВ,ГЗ,ЭЛ,СЧ
	}

	public static void main(String[] args) {
		String res[] = Parse(mail);
		System.out.println("Result:");
		for (int i=0; i<res.length; i++)
			System.out.println(res[i]);			
	}

	public static String[] Parse(String txt) {

		String result[] = new String[6];

		// Upper
		String raw = txt.toUpperCase();
		System.out.println("Upper:\n" + raw);

		// Replace
		Matcher replacer = r.matcher(raw);
		raw = replacer.replaceAll(" ");
		System.out.println("Replace:\n" + raw);

		// Search.
		// TODO: Recursion (GetScore('LS'),GetScore('HW'),...).
		Matcher m = p.matcher(raw);
		System.out.println("Founds: ");
		while (m.find()) {
			System.out.println(m.group(1) + ":" + m.group(2));
			ScoresEnum e = ScoresEnum.valueOf(m.group(1));
			switch (e) {
			case ЛС:
				if(result[LS_PARSE_POS]==null) result[LS_PARSE_POS] = m.group(2);
				break;
			case ГВ:
				if(result[HW_PARSE_POS]==null) result[HW_PARSE_POS] = m.group(2);
				break;
			case ХВ:
				if(result[CW_PARSE_POS]==null) result[CW_PARSE_POS] = m.group(2);
				break;			
			case ГЗ:
				if(result[GZ_PARSE_POS]==null) result[GZ_PARSE_POS] = m.group(2);
				break;
			case ЭЛ:
				if(result[EL_PARSE_POS]==null) result[EL_PARSE_POS] = m.group(2);
				break;
			case СЧ:
				if(result[SC_PARSE_POS]==null) result[SC_PARSE_POS] = m.group(2);
				break;
			default:
				break;
			}
		}

		return result;

	}
}
