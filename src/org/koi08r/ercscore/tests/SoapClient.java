package org.koi08r.ercscore.tests;

import javax.activation.DataHandler;

import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;

import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;

/*
http://www.theserverside.com/news/1365335/Introducing-Axiom
http://www.j2mesalsa.com/axis/axiomclient.php
http://today.java.net/pub/a/today/2006/12/13/invoking-web-services-using-apache-axis2.html
*/

public class SoapClient {

	private static EndpointReference targetEPR = new EndpointReference("http://localhost:8080/ercscore/services/ercscore");

	public static void main(String[] args) {

		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = fac.createOMNamespace("http://ws.ercscore.koi08r.org", "ns1");
		// OMNamespace ns = fac.createOMNamespace("http://www.w3.org/2001/XMLSchema", "xsd");
		// OMNamespace ns = fac.createOMNamespace("http://www.w3.org/2001/XMLSchema-instance", "xsi");
		// elem.addAttribute("xsi:type", "xsd:string", null);
		// SOAPFactory omfactory = OMAbstractFactory.getSOAP12Factory();
		// SOAPEnvelope reqEnv = omfactory.getDefaultEnvelope();
		// reqEnv.declareNamespace("http://www.w3.org/2001/XMLSchema", "xsd");

		OMElement payload = fac.createOMElement("Echo", ns);
		OMElement articleName = fac.createOMElement("str", ns);
		// articleName.addAttribute("xsi:type", "xsd:string", null); // ??
		//OMText txt = fac.createOMText("Hello", "image/png", true); // ??
		//articleName.addChild(txt);

		payload.addChild(articleName);
		articleName.setText("Introducing Axiom");

		System.out.println(payload.toString());

		Options options = new Options();
		options.setTo(targetEPR);
		// options.setAction("urn:echo");

		// ServiceClient dynamicClient = new ServiceClient(null, "http://localhost:8080/ercscore/services/ercscore?wsdl", "Echo", 8080);

		ServiceClient client = null;
		OMElement result = null;
		try {
			client = new ServiceClient();
			client.setOptions(options);
			result = client.sendReceive(payload); // Blocking (sendReceiveNonblocking).
			System.out.println(result.toString());
		} catch (AxisFault e) {
			e.printStackTrace();
		}

		System.out.println(MakeBinary().toString());
		System.out.println(createPayLoad().toString());

	}

	private static OMElement MakeBinary() {
		byte bytes[] = {1,2,3,4,5,6,7,8,9,0};
		ByteArrayDataSource ds = new ByteArrayDataSource(bytes);
		System.out.println("C/t: " + ds.getContentType());
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = fac.createOMNamespace("http://ws.ercscore.koi08r.org", "ns1");
		OMElement res = fac.createOMElement("res", ns);
		OMText data = fac.createOMText(new DataHandler(ds), true);
		res.addChild(data);
		return res;
	}

	public static OMElement createPayLoad() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace("http://ws.apache.org/axis2/xsd", "ns1");
		OMElement method = fac.createOMElement("echo", omNs);
		OMElement value = fac.createOMElement("value", omNs);
		value.setText("Hello , my first service utilization");
		method.addChild(value);
		return method;
	}

}
