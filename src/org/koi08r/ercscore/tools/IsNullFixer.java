package org.koi08r.ercscore.tools;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import au.com.bytecode.opencsv.CSVWriter;


public class IsNullFixer {

	private static Connection c = null;
	private static PreparedStatement st = null;
	private static ResultSet rs = null;
	private static String q =  "SELECT sc.ts,src.src,sc.raw FROM scores sc INNER JOIN sources src ON sc.src_id = src.id WHERE sc.ls_check = 1 AND sc.raw IS NOT NULL AND (sc.ls = 0 OR sc.ls IS NULL) ORDER BY sc.ts";
	private static final Pattern p = Pattern.compile("(ЛС|ГВ|ХВ|ГА?З?|ЭЛ|СЧ)\\s*([\\dЗОO]+)");
	private static final Pattern r = Pattern.compile("(?:[^А-Яа-я,\\.\\d]+|[\\n\\r\\s]+)");
	private static final Pattern s = Pattern.compile("\\s");
	private static final Pattern z = Pattern.compile("З");
	private static final Pattern o = Pattern.compile("O|О");
	private static final Pattern nl = Pattern.compile("\\n|\\r");
	private static final Pattern tz = Pattern.compile("^0+");
	private static CSVWriter csvw = null;

	private enum ScoresEnum {
		ЛС,ГВ,ХВ,ГЗ,ГАЗ,Г,ЭЛ,СЧ
	}

	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {

		Class.forName("com.mysql.jdbc.Driver");
		c = DriverManager.getConnection("jdbc:mysql://192.168.100.150:3306/score?user=score&password=score&useUnicode=true&characterEncoding=UTF-8&jdbcCompliantTruncation=false");
		st = c.prepareStatement(q);
		rs = st.executeQuery();

		csvw = new CSVWriter(new FileWriter("scores_lsnull.fix.csv"), ';');
		String[] headers = { "id","ts","src","ls","cw","hw","gz","el","sc","raw" };
		csvw.writeNext(headers);

		int count = 0;
		while (rs.next()) {
			count++;
			String txt = rs.getString("raw");
			SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyy");
			String d = df.format(rs.getDate("ts"));
			String src = rs.getString("src");
			System.out.println(count + " >>> " + "'" + txt + "'" + "->" + "'" + Parse(count,d,src,txt) + "'");
		}

		System.out.println("Count: " + count);

		if (rs != null)
			rs.close();
		st.close();
		c.close();

		csvw.close();
	}

	private static String Parse(int count, String ts, String src, String txt) {

		String ls = null;
		String hw = null;
		String cw = null;
		String gz = null;
		String el = null;
		String sc = null;

		// Upper
		String raw = txt.toUpperCase();

		// Replace
		Matcher replacer = r.matcher(raw);
		raw = replacer.replaceAll(" ");

		// Spacer
		Matcher spacer = s.matcher(raw);
		raw = spacer.replaceAll("");

		Matcher m = p.matcher(raw);
		while (m.find()) {
			// System.out.println(m.group(1) + ":" + m.group(2));
			ScoresEnum e = ScoresEnum.valueOf(m.group(1));
			switch (e) {
			case ЛС:
				if(ls==null) ls = normalize(m.group(2));
				else throw new IllegalArgumentException("Двойной ЛС");
				break;
			case ГВ:
				if(hw==null) hw = trailZeros( normalize(m.group(2)) );
				else throw new IllegalArgumentException("Двойной ГВ");
				break;
			case ХВ:
				if(cw==null) cw = trailZeros( normalize(m.group(2)) );
				else throw new IllegalArgumentException("Двойной ХВ");
				break;			
			case ГЗ:
				if(gz==null) gz = trailZeros( normalize(m.group(2)) );
				else throw new IllegalArgumentException("Двойной ГЗ");
				break;
			case ГАЗ:
				if(gz==null) gz = trailZeros( normalize(m.group(2)) );
				else throw new IllegalArgumentException("Двойной ГЗ");
				break;
			case Г:
				if(gz==null) gz = trailZeros( normalize(m.group(2)) );
				else throw new IllegalArgumentException("Двойной ГЗ");
				break;				
			case ЭЛ:
				if(el==null) el = trailZeros( normalize(m.group(2)) );
				else throw new IllegalArgumentException("Двойной ЭЛ");
				break;
			case СЧ:
				if(sc==null) sc = trailZeros( normalize(m.group(2)) );
				else throw new IllegalArgumentException("Двойной СЧ");
				break;
			default:
				break;
			}
		}

		// Del NL
		Matcher nliner = nl.matcher(txt);
		String[] entries = { String.valueOf(count),ts,src,ls,cw,hw,gz,el,sc,nliner.replaceAll("") };
		csvw.writeNext(entries);

		return
				"Дата: " + ts  + "; " +
				"ЛС: " + ls  + "; " +
				"ГВ: " + hw  + "; " +
				"ХВ: " + cw  + "; " +
				"ГЗ: " + gz  + "; " +
				"ЭЛ: " + el  + "; " +
				"СЧ: " + sc  +
				"";
	}

	private static String normalize(String in) {
		String out = in;
		// О|З
		Matcher zl = z.matcher(out);
		out = zl.replaceAll("3");

		// О|З
		Matcher ol = o.matcher(out);
		out = ol.replaceAll("0");
		
		return out;
	}

	private static String trailZeros(String in) {
		String out = in;

		Matcher z = tz.matcher(out);
		out = z.replaceAll("");

		return out;
	}

}
