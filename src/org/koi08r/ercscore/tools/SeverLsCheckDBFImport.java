package org.koi08r.ercscore.tools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.xBaseJ.DBF;
import org.xBaseJ.fields.CharField;
import org.xBaseJ.fields.NumField;

public class SeverLsCheckDBFImport {

	private static Connection conn = null;
	private static String DbfFile = "help/Reestr_MRC_12.10.12.dbf";

	private static Connection Connect() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		conn = DriverManager.getConnection("jdbc:mysql://192.168.100.150:3306/score", "score", "score");
		return conn;
	}

	private static int insertRecord(String ls) throws SQLException, ClassNotFoundException {
		PreparedStatement st = null;
		if (conn == null)
			Connect();

		st = conn.prepareStatement("INSERT IGNORE INTO ls_check (ls, hw, cw, gz, el) VALUES (?, 1, 1, 1, 1)");
		st.setString(1, ls);
		return st.executeUpdate();
	}

	public static void main(String[] args) {

		try {
			DBF db = new DBF(DbfFile, DBF.READ_ONLY, "CP866"); 

			NumField ls = (NumField) db.getField("LS");
			CharField adr  = (CharField) db.getField("ADR");

			int rc = db.getRecordCount();
			// int rc = 10;
			for (int i = 1; i <= rc; i++) {
				db.read();
				String _ls = ls.get().trim();
				String _adr = adr.get().trim();
				System.out.println(_ls + "@" + "'" + _adr + "'");
				insertRecord(_ls);
			}

		} catch(Exception e) {
			e.printStackTrace();
		}

	}

}
