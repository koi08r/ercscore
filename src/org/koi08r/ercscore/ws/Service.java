package org.koi08r.ercscore.ws;

import java.io.UnsupportedEncodingException;

import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.description.AxisService;
import org.apache.axis2.engine.ServiceLifeCycle;
import org.apache.log4j.Logger;
import org.koi08r.ercscore.helpers.DbHelper;
import org.koi08r.ercscore.helpers.DbLimits;
import org.koi08r.ercscore.parsers.Schetchik;
import org.apache.commons.codec.binary.Base64;

public class Service implements ServiceLifeCycle {

	public final static int IMPORT_ERR_SUCCESS = 1;
	public final static int IMPORT_ERR_LEN = 254;
	public final static int IMPORT_ERR_SYS = 255;
	public final static int IMPORT_ERR_FORMAT = 128;
	public final static int IMPORT_ERR_LSFOUND = 101;
	public final static int IMPORT_ERR_HWFOUND = 102;
	public final static int IMPORT_ERR_CWFOUND = 103;
	public final static int IMPORT_ERR_GZFOUND = 104;
	public final static int IMPORT_ERR_ELFOUND = 105;
	public final static int IMPORT_ERR_SCFOUND = 106;

	private Logger loger = Logger.getLogger("ercscore");


	/*
	 * Тестовые функции типа ping.
	 */

	public String Echo(String str) {
		return str;
	}
	public String Hello() {
		return "Hello";
	}



	/*
	 * Основные функции.
	 */

	public String pushScore(String src, String agent, String ls, String hw, String cw, String gz, String el, String sc, String raw) {
		String res = "ok";
		loger.info("@@@ Call pushScore (" + src + "','" + agent + "','" + ls + "','" + hw + "','" + cw + "','" + gz + "','" + el + "','" + sc + "','" + raw + ")");

		if (! DbLimits.checkParams( src,agent,ls,hw,cw,gz,el,sc,raw )) {
			loger.error("Wrong length");
			res = "Wrong length";
		} else {
			DbHelper db = DbHelper.getInstance();
			try {
				db.pushScore(IMPORT_ERR_SUCCESS, src, agent, ls, hw, cw, gz, el, sc, raw);
			} catch (IllegalStateException e) {
				loger.error("Can't sql insert data: '" + e.toString() + "'");
				res = "Can't sql insert data: '" + e.toString() + "'";
			}
		}

		// String attr = (String) getServletContext().getAttribute("TestAttr");
		// loger.info("Test attribute is: " + attr);

		loger.info("Result pushScore: " + res);
		return res;
	}

	public int[] checkLs(String ls) {
		int res[] = new int[4];
		loger.info("@@@ Call checkLs ('" + ls + "')");

		if(! DbLimits.checkParams(null,null,ls,null,null,null,null,null,null))
			return null;

		try {
			res = DbHelper.getInstance().checkLs(ls);
		} catch (IllegalStateException e) {
			loger.error("Can't sql select: '" + e.toString() + "'");
			return null;
		}

	return res;
	}

	// TODO: Заменить индексы на именованые константы.
	public int importScore(String src, String agent, String raw) {
		int res = IMPORT_ERR_SUCCESS;

		loger.info("@@@ Call importScore (" + "'" + src + "','" + agent + "','" + raw + "')");

		// raw ???
		String decoded;
		byte[] decodedBytes;
		try {
			decodedBytes = Base64.decodeBase64(raw.getBytes());
		} catch (NullPointerException e) {
			loger.error("Raw text is null: '" + e.toString() + "'");
			return IMPORT_ERR_LSFOUND;
		}
		try {
			decoded = new String(decodedBytes,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			loger.error("Can't convert charset: '" + e.toString() + "'");
			return IMPORT_ERR_LSFOUND;
		}

		// scores[] = {ЛС,ГВ,ХВ,ГЗ,ЭЛ,СЧ}
		String scores[] = Schetchik.getInstance().ParseScore(decoded);

		// TODO: scores[Schetchik.LS_PARSE_POS]
		if (! DbLimits.checkParams( null,null,scores[0],scores[1],scores[2],scores[3],scores[4],scores[5],null ))
			return IMPORT_ERR_LEN;

		DbHelper db = DbHelper.getInstance();
		try {
			// ls_check[] = {ГВ,ХВ,ГЗ,ЭЛ}
			int ls_check[] = DbHelper.getInstance().checkLs(scores[0]);

			if (ls_check == null)
				res = IMPORT_ERR_LSFOUND;
			else {
				if      (ls_check[0] == 0 && scores[1] != null) // HW
					res = IMPORT_ERR_HWFOUND;
				else if (ls_check[1] == 0 && scores[2] != null) // CW
					res = IMPORT_ERR_CWFOUND;
				else if (ls_check[2] == 0 && scores[3] != null) // GZ
					res = IMPORT_ERR_GZFOUND;
				else if (ls_check[3] == 0 && scores[4] != null) // EL
					res = IMPORT_ERR_ELFOUND;
				else {} // OK
			}

			db.pushScore(res, src, agent, scores[0], scores[1], scores[2], scores[3], scores[4], scores[5], decoded);

		} catch (IllegalStateException e) {
			loger.error("Can't sql: '" + e.toString() + "'");
			// TODO: Костыль ?
			// if( e.toString().endsWith("MysqlDataTruncation") ) {
			//	res = IMPORT_ERR_LSFOUND; // Формат !	
			// } else
			res = IMPORT_ERR_SYS;
		}

		loger.info("Result importScore: " + res);
		return res;
	}


	/*
	 * Вспомогательные функции.
	 */

	public static final String StrError(int lscheck) {
		String result = null;
		switch (lscheck) {
		case IMPORT_ERR_SUCCESS:
			result = "Показания приняты";
			break;
		case IMPORT_ERR_LEN:
			result = "Неверная длина";
			break;
		case IMPORT_ERR_SYS:
			result = "Системная ошибка";
			break;
		case IMPORT_ERR_FORMAT:
			result = "Неверный формат";
			break;
		case IMPORT_ERR_LSFOUND:
			result = "Лс не найден";
			break;
		case IMPORT_ERR_HWFOUND:
			result = "Счетчик ГВ не найден";
			break;
		case IMPORT_ERR_CWFOUND:
			result = "Счетчик ХВ не найден";
			break;
		case IMPORT_ERR_GZFOUND:
			result = "Счетчик ГЗ не найден";
			break;
		case IMPORT_ERR_ELFOUND:
			result = "Счетчик ЭЛ не найден";
			break;
		case IMPORT_ERR_SCFOUND:
			result = "Счетчик СЧ не найден";
			break;
		default:
			break;
		}
		return result;
	}

	/*
	 * Системные функции.
	 */


	public void shutDown(ConfigurationContext configctx, AxisService service) {
		loger.info(service.getName() + " is shutting down.");
	}

	public void startUp(ConfigurationContext configctx, AxisService service) {
		loger.info(service.getName() + " is starting up.");
	}

	// private ServletContext getServletContext() {
	//	MessageContext mc = MessageContext.getCurrentMessageContext();
		// HttpServlet servlet = (HttpServlet)mc.getProperty(HTTPConstants.MC_HTTP_SERVLET);
		// ServletContext servletContext = servlet.getServletContext();
	//	return (ServletContext) mc.getProperty(HTTPConstants.MC_HTTP_SERVLETCONTEXT);
	// }

}
